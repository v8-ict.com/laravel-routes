# Laravel Routes
[![Latest Stable Version](https://poser.pugx.org/bluedragon/laravel-routes/v/stable)](https://packagist.org/packages/bluedragon/laravel-routes)
[![pipeline status](https://gitlab.com/blue-dragon/laravel-routes/badges/master/pipeline.svg)](https://gitlab.com/blue-dragon/laravel-routes/pipelines)
[![coverage report](https://gitlab.com/blue-dragon/laravel-routes/badges/master/coverage.svg)](https://gitlab.com/blue-dragon/laravel-routes/pipelines)
[![License](https://poser.pugx.org/bluedragon/laravel-routes/license)](https://packagist.org/packages/bluedragon/laravel-routes)

Laravel Routes offers you a flexible package to make Laravel routes available in JavaScript. With Laravel Routes you are able to split routes into groups and only load the routes needed in JavaScript. You are in control.

## Quick start
* Run `composer require bluedragon/laravel-routes --dev`
* Add `laroute_publish => true` to the routes you want to be able to access in JavaScript.
```php
Route::get('/', ['uses' => function () {
    return view('welcome');
}, 'laroute_publish' => true])
->name('home');
```
* Run `php artisan laravel-routes:publishall`.
* If you haven't already run `php artisan storage:link`.
* Add the next part to your html.
```html
<script type="text/javascript" src="{{ asset('/storage/laravel_routes/laravel_routes.js') }}"></script>
```
* Load the Laravel Routes file via the following function:
```javascript
const laravelRoutes = new LaravelRoutes('{{ asset('/storage/laravel_routes/routes.json')  }}');
```
* You can retrieve a route via:
```javascript
laravelRoutes.route('route.name', {parameterName: parameterValue});
```

## Installation
Laravel Routes is available via composer. Install Laravel Routes by executing: composer require bluedragon/laravel-routes --dev. We suggest installing this package as a dev dependency. The Laravel Routes package is not needed in the production environment because all files will generated during development.

### Auto providers
By default this package will be auto discovered. If you have disabled autodiscovery you need to add BlueDragon\LaravelRoutes\LaravelRoutesServiceProvider::class, to the providers array in config/app.php and make sure it is only loaded during development.

## Configuration
To have all configuration options available, you can publish the config file with php artisan vendor:publish. The config file is is commented and self-explanatory.

### Configurate the routes to publish.
**Please note that a route without a name will never be published.**

#### Routes file  
There are different ways to publish a route. The easiest way is to add 'laroute_publish' => true to the route.
```php
Route::get('/', ['uses' => function () {
    return view('welcome');
}, 'laroute_publish' => true])
->name('home');
```

It is also possible to use ‘laroute_publish’ => true on a routesgroup.  Within a group you are able to overwrite the value for a specific route within the group as shown below.
```php
Route::group(['laroute_publish' => true], function () {
    Route::get('/', function () {
        return view('welcome');
    })->name('home');

    Route::get('/hidden', ['uses' => function () {
        return view('welcome');
    }, 'laroute_publish' => false])
    ->name('hidden');

    Route::get('/contact', function () {
        return view('contact');
    })->name('contact');
});

```

#### In config file
The third option is to define the routes in a config file. Any setting in the routes file will overwrite the config value. The definition is done in route groups, by default we use the default group.
```php
//config/laravel-routes.php
    'route_groups' => [
        'default' => [ // the group name
            'include_true' => true, // if we include the routes with 'laroute_publish' => true
            'routes' => [ // the routes we want to add to the group
                'home' => true,
                'resource.show' => true,
            ],
        ],
    ],
```

### Route groups
Especially in bigger projects it can be usefull to split the routes you want to use in JavaScript into groups. If you have a webapplication with e.g. a CMS, you can decide to split the routes into groups. This way, the CMS routes will only load in the CMS part of your webapplication and are excluded from the frontend routes. 

There are several ways to add routes to groups. Adding routes to groups can be done by adding them to the config file in their own group.
```php
//config/laravel-routes.php
    'route_groups' => [
        'default' => [ // the group name
            'include_true' => true, // if we include the routes with 'laroute_publish' => true
            'routes' => [ // the routes we want to add to the group
            ],
        ],
        'cms' => [ // the group name
            'include_true' => false, // if we include the routes with 'laroute_publish' => true
            'routes' => [ // the routes we want to add to the group
                'cms.pages.index' => true,
                'cms.pages.create' => true,
                'cms.pages.store' => true,
                '...' => true,
            ],
        ],
    ],
```
Another way is to define the groupname on the route (group) in the routes file
```php
Route::get('/cms/pages', ['uses' => function () {
    return view('cms.pages.index');
}, 'laroute_publish' => 'cms'])
->name('cms.pages.index');
```

If you have multiple `route_groups` defined in the config and you want to access a route in both groups, in the config file set `include_true` to `true`. And in the routes file add `laroute_publish' => true` to the route that you want to access in both groups.

**It is not needed to define a group in the config file. If there is no group defined the routes will not be published upon executing the laravel-routes:publishall command.**

### Change the location of the files.
By default the files are stored public/laravel_routes/, this can be changed with in the config file. The option to change the path is: `export_directory`.

## Commands
We have three commands available with this package.

| Artisan command | function |
|----|------|
| laravel-routes:publishscript {--filename=} {--path=} | This publish the file with the javascript functions. |
| laravel-routes:publishroutes {--filename=} {--path=} {--group=} | This publish the routes for a group, by default the default group|
| laravel-routes:publishall {--path=} | This publish the javascript functions file and the route files for all the groups that are in the config. |


## Alternatives
If you have an older Laravel version you can use [lord/laroute](https://packagist.org/packages/lord/laroute). Laroute is the package that inspired to create this one. Other possibly useful similar packages could be:
* [Ziggy](https://github.com/tightenco/ziggy)
* [laravel-js-routes](https://www.npmjs.com/package/laravel-js-routes)

## Migrating from lord/laroute
If you are currently using [lord/laroute](https://packagist.org/packages/lord/laroute) you can use the Laravel Routes package with some minor configuration changes. After publishing the config file you need to change 'route_keyname' => 'laroute_publish', to 'route_keyname' => 'laroute',. This way you don't have to change all the routes.In contrast to [lord/laroute](https://packagist.org/packages/lord/laroute) we explicitly don't support the option to publish all routes by default. If you want to publish all routes by default, you need to place all the routes in a route group.
```php
Route::group(['laroute' => true], function () {
    // everything from your routes file
});

``` 
For the JavaScript part of the migration we can load a routes file via the following code:
```javascript
const laroute = new LaravelRoutes('{{ asset('/storage/laravel_routes/routes.json')  }}');
```
To retrieve a route the following function can be called:
```javascript
laroute.route('route.name', {parameterName: parameterValue});
```

## Changelog
We (try to) document all the changes in [CHANGELOG](CHANGELOG.md). Please consult it for more information.

## Contributing
You are very welcome to contribute, read about it in [CONTRIBUTING](CONTRIBUTING.md)

## License
The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Made by Blue Dragon
This package is developed by [Blue Dragon Digital Technology](https://www.bluedragon-dt.nl/ "Blue Dragon Digital Technology"). Established in 1999, Blue Dragon Digital Technology has been providing leading digital solutions ranging from webapplications and ecommerce solutions to mobile apps. We are part of the [Blue Dragon](https://www.bluedragon.nl/ "Blue Dragon") family. You’ll find us where creativity, technology and connectivity intersect. Where everything, and we mean everything, is possible to help communication make an impact.

[![Blue Dragon](https://gitlab.com/blue-dragon/laravel-routes/raw/master/logo_blue_dragon.png)](https://www.bluedragon.nl)