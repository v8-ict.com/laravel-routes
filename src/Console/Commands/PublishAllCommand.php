<?php

namespace BlueDragon\LaravelRoutes\Console\Commands;

use BlueDragon\LaravelRoutes\Exceptions\LaravelRoutesException;
use BlueDragon\LaravelRoutes\Publishers\FunctionsPublisherInterface;
use BlueDragon\LaravelRoutes\Publishers\RoutesPublisherInterface;
use Illuminate\Config\Repository;
use Illuminate\Console\Command;

/**
 * Class PublishRoutesCommands
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 */
class PublishAllCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-routes:publishall {--path=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish the function file and the group files';

    /**
     * @param RoutesPublisherInterface $publisher
     *
     * @return int
     */
    public function handle(
        Repository $config,
        FunctionsPublisherInterface $functionsPublisher,
        RoutesPublisherInterface $routesPublisher
    ) : int {
        $groups = array_keys($config->get('laravel-routes.route_groups', []));
        try {
            if ($this->input->isInteractive()) {
                $this->output->progressStart(count($groups) + 1);
            }

            $functionsPublisher->publish(null, $this->option('path'));
            if ($this->input->isInteractive()) {
                $this->output->progressAdvance();
            }

            foreach ($groups as $group) {
                $routesPublisher->publish($group, null, $this->option('path'));
                if ($this->input->isInteractive()) {
                    $this->output->progressAdvance();
                }
            }

            if ($this->input->isInteractive()) {
                $this->output->progressFinish();
            }

            $this->info('Routes and functions published');
            return 0;
        } catch (LaravelRoutesException $exception) {
            $this->error('Sorry we have an exception: ' . $exception->getMessage());
            return 1;
        }
    }
}
