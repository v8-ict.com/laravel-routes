<?php

namespace BlueDragon\LaravelRoutes\Console\Commands;

use BlueDragon\LaravelRoutes\Exceptions\LaravelRoutesException;
use BlueDragon\LaravelRoutes\Publishers\FunctionsPublisherInterface;
use Illuminate\Console\Command;

/**
 * Class PublishFunctionsCommand
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 */
class PublishFunctionsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-routes:publishscript {--filename=} {--path=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish the file with the functions';

    /**
     * Execute the console command.
     *
     * @param FunctionsPublisherInterface $publisher
     *
     * @return int
     */
    public function handle(FunctionsPublisherInterface $publisher) : int
    {
        try {
            $publisher->publish($this->option('filename'), $this->option('path'));
            $this->info('File published');
            return 0;
        } catch (LaravelRoutesException $exception) {
            $this->error('Sorry we have an exception: ' . $exception->getMessage());
            return 1;
        }
    }
}
