<?php

namespace BlueDragon\LaravelRoutes\Console\Commands;

use BlueDragon\LaravelRoutes\Exceptions\LaravelRoutesException;
use BlueDragon\LaravelRoutes\Publishers\RoutesPublisherInterface;
use Illuminate\Console\Command;

/**
 * Class PublishRoutesCommands
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 */
class PublishRoutesCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-routes:publishroutes {--filename=} {--path=} {--group=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish the file with the routes for a group';

    /**
     * @param RoutesPublisherInterface $publisher
     *
     * @return int
     */
    public function handle(RoutesPublisherInterface $publisher) : int
    {
        try {
            $publisher->publish($this->option('group'), $this->option('filename'), $this->option('path'));
            $this->info('Routes published');
            return 0;
        } catch (LaravelRoutesException $exception) {
            $this->error('Sorry we have an exception: ' . $exception->getMessage());
            return 1;
        }
    }
}
