<?php

namespace BlueDragon\LaravelRoutes\Creators;

use Illuminate\Contracts\Config\Repository;
use Illuminate\View\Factory;

/**
 * Class FunctionCreator
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 */
class FunctionCreator implements FunctionCreatorInterface
{
    /**
     * @var \Illuminate\Config\Repository
     */
    protected $config;

    /**
     * @var Factory
     */
    protected $viewFactory;

    /**
     * FunctionsPublisher constructor.
     *
     * @param Repository $config
     * @param Factory $viewFactory
     */
    public function __construct(Repository $config, Factory $viewFactory)
    {
        $this->config = $config;
        $this->viewFactory = $viewFactory;
    }

    /**
     * Get the script with the functions
     *
     * @return string
     */
    public function getScript() : string
    {
        return $this->viewFactory
            ->make($this->config->get('laravel-routes.script_functions_template'))
            ->render();
    }
}
