<?php

namespace BlueDragon\LaravelRoutes\Creators;

/**
 * Interface FunctionCreatorInterface
 */
interface FunctionCreatorInterface
{
    /**
     * Get the script with the functions
     *
     * @return string
     */
    public function getScript() : string;
}
