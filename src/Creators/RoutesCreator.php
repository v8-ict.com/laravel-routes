<?php


namespace BlueDragon\LaravelRoutes\Creators;

/**
 * Class RoutesCreator
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 */
class RoutesCreator implements RoutesCreatorInterface
{

    /**
     * @var RoutesDataCreatorInterface
     */
    protected $dataCreator;

    /**
     * RoutesCreator constructor.
     *
     * @param RoutesDataCreatorInterface $dataCreator
     */
    public function __construct(RoutesDataCreatorInterface $dataCreator)
    {
        $this->dataCreator = $dataCreator;
    }

    /**
     * @param string $group
     *
     * @return string
     */
    public function getRoutesScript(string $group) : string
    {
        $data = [
            'routes' => $this->dataCreator->getRoutesData($group)->toArray(),
        ];
        return json_encode($data);
    }
}
