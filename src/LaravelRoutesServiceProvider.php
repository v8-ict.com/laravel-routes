<?php

namespace BlueDragon\LaravelRoutes;

use BlueDragon\LaravelRoutes\Console\Commands\PublishAllCommand;
use BlueDragon\LaravelRoutes\Console\Commands\PublishFunctionsCommand;
use BlueDragon\LaravelRoutes\Console\Commands\PublishRoutesCommands;
use BlueDragon\LaravelRoutes\Creators\RoutesCreatorInterface;
use BlueDragon\LaravelRoutes\Creators\RoutesDataCreatorInterface;
use BlueDragon\LaravelRoutes\Publishers\FunctionsPublisherInterface;
use BlueDragon\LaravelRoutes\Creators\FunctionCreatorInterface;
use BlueDragon\LaravelRoutes\Publishers\RoutesPublisherInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

/**
 * Class LaravelRoutesServiceProvider
 *
 * Provide all the services needed for the laravel-routes package
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 */
class LaravelRoutesServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot() : void
    {
        $this->publishes([
            __DIR__.'/../config/laravel-routes.php' => config_path('laravel-routes.php'),
        ]);
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'laravel-routes');

        if ($this->app->runningInConsole()) {
            $this->commands([
                PublishAllCommand::class,
                PublishFunctionsCommand::class,
                PublishRoutesCommands::class,
            ]);
        }
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register() : void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/laravel-routes.php', 'laravel-routes');

        $this->app->bind(FunctionsPublisherInterface::class, config('laravel-routes.functions_publisher'));
        $this->app->bind(FunctionCreatorInterface::class, config('laravel-routes.functions_creator'));

        $this->app->bind(RoutesCreatorInterface::class, config('laravel-routes.routes_creator'));
        $this->app->bind(RoutesDataCreatorInterface::class, config('laravel-routes.routes_data_creator'));
        $this->app->bind(RoutesPublisherInterface::class, config('laravel-routes.routes_publisher'));
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() : array
    {
        return [
            FunctionCreatorInterface::class,
            FunctionsPublisherInterface::class,
            RoutesCreatorInterface::class,
            RoutesDataCreatorInterface::class,
            RoutesPublisherInterface::class,
        ];
    }
}
