<?php

namespace BlueDragon\LaravelRoutes\Publishers;

use BlueDragon\LaravelRoutes\Exceptions\LaravelRoutesException;

/**
 * Interface FunctionsPublisherInterface
 */
interface FunctionsPublisherInterface
{
    /**
     * Publish the functions file
     *
     * @param string|null $filename
     * @param string|null $path
     *
     * @throws LaravelRoutesException
     *
     * @return void
     */
    public function publish(string $filename = null, string $path = null) : void;
}
