<?php


namespace BlueDragon\LaravelRoutes\Publishers;

use BlueDragon\LaravelRoutes\Creators\RoutesCreatorInterface;
use BlueDragon\LaravelRoutes\Exceptions\LaravelRoutesException;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Filesystem\Filesystem;

/**
 * Class RoutesPublisher
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 */
class RoutesPublisher implements RoutesPublisherInterface
{
    /**
     * @var \Illuminate\Config\Repository
     */
    protected $config;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var RoutesCreatorInterface
     */
    protected $routesCreator;

    /**
     * RoutesPublisher constructor.
     *
     * @param Repository $config
     * @param Filesystem $filesystem
     * @param RoutesCreatorInterface $routesCreator
     */
    public function __construct(Repository $config, Filesystem $filesystem, RoutesCreatorInterface $routesCreator)
    {
        $this->config = $config;
        $this->filesystem = $filesystem;
        $this->routesCreator = $routesCreator;
    }

    /**
     * Publish the functions file
     *
     * @param string|null $group
     * @param string|null $filename
     * @param string|null $path
     *
     * @throws LaravelRoutesException
     *
     * @return void
     */
    public function publish(string $group = null, string $filename = null, string $path = null) : void
    {
        if ($group === null) {
            $group = $this->config->get('laravel-routes.default_group');
        }
        if ($filename === null) {
            $filename = $this->config->get('laravel-routes.routes_file_basename');

            if ($group !== $this->config->get('laravel-routes.default_group')) {
                $filename .= '_' . $group;
            }

            $filename .= '.'.$this->config->get('laravel-routes.routes_file_extension');
        }
        if ($path === null) {
            $path = $this->config->get('laravel-routes.export_directory');
        }

        try {
            $this->filesystem->makeDirectory($path, 0755, true, true);
            $this->filesystem
                ->put(
                    $path . DIRECTORY_SEPARATOR . $filename,
                    $this->routesCreator->getRoutesScript($group)
                );
        } catch (\Throwable $error) {
            throw new LaravelRoutesException('Something went wrong', 0, $error);
        }
    }
}
