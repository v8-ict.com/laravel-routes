<?php

namespace BlueDragon\LaravelRoutes\Publishers;

use BlueDragon\LaravelRoutes\Exceptions\LaravelRoutesException;

/**
 * Interface RoutesPublisherInterface
 */
interface RoutesPublisherInterface
{
    /**
     * Publish the routes file
     *
     * @param string|null $group
     * @param string|null $filename
     * @param string|null $path
     *
     * @throws LaravelRoutesException
     *
     * @return void
     */
    public function publish(string $group = null, string $filename = null, string $path = null) : void;
}
