<?php

namespace BlueDragon\LaravelRoutes\Tests\Commands;

use BlueDragon\LaravelRoutes\Exceptions\LaravelRoutesException;
use BlueDragon\LaravelRoutes\Publishers\FunctionsPublisherInterface;
use BlueDragon\LaravelRoutes\Publishers\RoutesPublisherInterface;
use BlueDragon\LaravelRoutes\Tests\TestCase;
use Illuminate\Config\Repository;

/**
 * Class PublishAllTest
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 *
 * @group commands
 */
class PublishAllTest extends TestCase
{

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_can_publish_the_script_and_routes(): void
    {
        $this->mock(FunctionsPublisherInterface::class, function ($mock) {
            $mock->shouldReceive('publish')
                ->with(null, null)
                ->once();
        });

        $this->mock(RoutesPublisherInterface::class, function ($mock) {
            $mock->shouldReceive('publish')
                ->twice()
            ;
        });

        $this->mock(Repository::class, function ($mock) {
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_groups', [])
                ->once()
                ->andReturn([
                    'group1' => [
                       'config'
                    ],
                    'group2' => [
                        'config'
                    ],
                ]);
        });

        $this->artisan('laravel-routes:publishall')
            ->expectsOutput('Routes and functions published')
            ->assertExitCode(0)
        ;
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function the_publish_command_will_not_fail_if_there_isnt_any_group()
    {
        $this->mock(FunctionsPublisherInterface::class, function ($mock) {
            $mock->shouldReceive('publish')
                ->with(null, null)
                ->once();
        });

        $this->mock(RoutesPublisherInterface::class, function ($mock) {
            $mock->shouldReceive('publish')
                ->never()
            ;
        });

        $this->mock(Repository::class, function ($mock) {
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_groups', [])
                ->once()
                ->andReturn([]);
        });

        $this->artisan('laravel-routes:publishall')
            ->expectsOutput('Routes and functions published')
            ->assertExitCode(0)
        ;
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_get_a_nice_error_if_something_failed()
    {
        $this->mock(FunctionsPublisherInterface::class, function ($mock) {
            $mock->shouldReceive('publish')
                ->once()
                ->with(null, null)
                ->andThrow(new LaravelRoutesException('test exception'));
        });

        $this->mock(RoutesPublisherInterface::class, function ($mock) {
            $mock->shouldReceive('publish')
                ->never()
            ;
        });

        $this->artisan('laravel-routes:publishall')
            ->expectsOutput('Sorry we have an exception: test exception')
            ->assertExitCode(1)
        ;
    }
}