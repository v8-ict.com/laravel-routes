<?php

namespace BlueDragon\LaravelRoutes\Tests\Commands;

use BlueDragon\LaravelRoutes\Exceptions\LaravelRoutesException;
use BlueDragon\LaravelRoutes\Publishers\FunctionsPublisherInterface;
use BlueDragon\LaravelRoutes\Tests\TestCase;

/**
 * Class PublishFunctionsTest
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 *
 * @group commands
 * @group functions
 */
class PublishFunctionsTest extends TestCase
{
    /**
     *
     * @return void
     *
     * @test
     */
    public function we_can_call_the_command_with_default_options(): void
    {
        $this->mock(FunctionsPublisherInterface::class, function ($mock) {
            $mock->shouldReceive('publish')
                ->with(null, null)
                ->once();
        });
        $this->artisan('laravel-routes:publishscript')
            ->expectsOutput('File published')
            ->assertExitCode(0)
        ;
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_get_a_nice_error_if_something_failed(): void
    {
        $this->mock(FunctionsPublisherInterface::class, function ($mock) {
            $mock->shouldReceive('publish')
                ->once()
                ->with(null, null)
                ->andThrow(new LaravelRoutesException('test exception'));
        });
        $this->artisan('laravel-routes:publishscript')
            ->expectsOutput('Sorry we have an exception: test exception')
            ->assertExitCode(1)
        ;
    }
}
