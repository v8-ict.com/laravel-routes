<?php

namespace BlueDragon\LaravelRoutes\Tests\Creators;

use BlueDragon\LaravelRoutes\Creators\FunctionCreator;
use BlueDragon\LaravelRoutes\Tests\TestCase;

/**
 * Class FunctionTest
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 *
 * @group creators
 * @group functions
 */
class FunctionTest extends TestCase
{
    /**
     *
     * @return void
     *
     * @test
     */
    public function we_get_the_script(): void
    {
        $creator = app(FunctionCreator::class);

        $result = $creator->getScript();
        $this->assertNotEmpty($result);
    }

}
