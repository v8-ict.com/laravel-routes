<?php


namespace BlueDragon\LaravelRoutes\Tests\Creators;


use BlueDragon\LaravelRoutes\Creators\RoutesCreator;
use BlueDragon\LaravelRoutes\Creators\RoutesDataCreatorInterface;
use BlueDragon\LaravelRoutes\Tests\TestCase;

/**
 * Class RoutesTest
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 *
 * @group creators
 * @group routes
 */
class RoutesTest extends TestCase
{
    /**
     *
     * @return void
     *
     * @test
     */
    public function we_get_a_set_with_empy_routes_if_there_isnt_any_route(): void
    {
        $dataCreator = $this->mock(RoutesDataCreatorInterface::class, function ($mock){
            $mock->shouldReceive('getRoutesData')
                ->once()
                ->andReturn(collect());
        });
        $helper = new RoutesCreator($dataCreator);
        $result = $helper->getRoutesScript('default');
        $this->assertJson($result);
        $resultArray = json_decode($result, true);

        $this->assertArrayHasKey('routes', $resultArray);
        $this->assertEmpty($resultArray['routes']);
    }
}