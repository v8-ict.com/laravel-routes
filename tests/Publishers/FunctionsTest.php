<?php

namespace BlueDragon\LaravelRoutes\Tests\Publishers;

use BlueDragon\LaravelRoutes\Exceptions\LaravelRoutesException;
use BlueDragon\LaravelRoutes\Publishers\FunctionsPublisher;
use BlueDragon\LaravelRoutes\Creators\FunctionCreatorInterface;
use BlueDragon\LaravelRoutes\Tests\TestCase;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Filesystem\Filesystem;

/**
 * Class FunctionsTest, we test that we can publish the file with the javascript functions
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 *
 * @group publishers
 * @group functions
 */
class FunctionsTest extends TestCase
{
    /**
     *
     * @return void
     *
     * @test
     */
    public function we_can_publish_the_functions(): void
    {
        $config = $this->mock(Repository::class, function ($mock){
            $mock->shouldReceive('get')
                ->with('laravel-routes.script_functions_filename')
                ->once()
                ->andReturn('filename')
            ;
            $mock->shouldReceive('get')
                ->with('laravel-routes.export_directory')
                ->once()
                ->andReturn('path')
            ;
        });
        $creator = $this->mock(FunctionCreatorInterface::class, function ($mock) {
            $mock->shouldReceive('getScript')
                ->once()
                ->andReturn('testscript')
            ;
        });

        $filesystem = $this->mock(Filesystem::class, function ($mock) {
            $mock->shouldReceive('makeDirectory');
            $mock->shouldReceive('put')
                ->with('path' . DIRECTORY_SEPARATOR . 'filename', 'testscript')
                ->once()
            ;
        });

        $publisher = new FunctionsPublisher($config, $creator, $filesystem);

        $publisher->publish();
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_use_the_optional_filename_and_path_commands(): void
    {
        $config = $this->mock(Repository::class, function ($mock){
            $mock->shouldReceive('get')
                ->with('laravel-routes.script_functions_filename')
                ->never()
            ;
            $mock->shouldReceive('get')
                ->with('laravel-routes.export_directory')
                ->never()
            ;
        });
        $creator = $this->mock(FunctionCreatorInterface::class, function ($mock) {
            $mock->shouldReceive('getScript')
                ->once()
                ->andReturn('testscript')
            ;
        });

        $filesystem = $this->mock(Filesystem::class, function ($mock) {
            $mock->shouldReceive('makeDirectory');
            $mock->shouldReceive('put')
                ->with('custompath' . DIRECTORY_SEPARATOR . 'customfile', 'testscript')
                ->once()
            ;
        });

        $publisher = new FunctionsPublisher($config, $creator, $filesystem);

        $publisher->publish('customfile', 'custompath');
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_convert_any_error_to_a_package_exception(): void
    {
        $this->expectException(LaravelRoutesException::class);

        $config = $this->mock(Repository::class, function ($mock){
            $mock->shouldReceive('get')
                ->with('laravel-routes.script_functions_filename')
                ->once()
                ->andReturn('filename')
            ;
            $mock->shouldReceive('get')
                ->with('laravel-routes.export_directory')
                ->once()
                ->andReturn('path')
            ;
        });
        $creator = $this->mock(FunctionCreatorInterface::class);

        $filesystem = $this->mock(Filesystem::class, function ($mock) {
            $mock->shouldReceive('makeDirectory')
                ->once()
                ->andThrow(new \Exception('test'))
            ;
        });

        $publisher = new FunctionsPublisher($config, $creator, $filesystem);

        $publisher->publish();
    }
}
