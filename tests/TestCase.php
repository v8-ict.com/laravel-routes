<?php

namespace BlueDragon\LaravelRoutes\Tests;

/**
 * Class TestCase, the base TestCase for all the tests in this project
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 */
class TestCase extends \Orchestra\Testbench\TestCase
{
    /**
     * @param \Illuminate\Foundation\Application $app
     *
     * @return array
     */
    protected function getPackageProviders($app): array
    {
        return ['BlueDragon\LaravelRoutes\LaravelRoutesServiceProvider'];
    }
}
